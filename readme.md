# Sample Laravel Project

A simple application for tracking a list of projects

## Step 1 create a Model
Use migrations to create a table in sqlite for projects that stores the following data:
Project 'Number', Project Name, Project Manager, Project Description, Project budget

Once the model exits use artisan's tinker feature to check that it works

## Step 2 create a controller and basic views
Create a controller to view all projects and individual projects.
