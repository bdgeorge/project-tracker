<?php
/**
 * @author  Ben George <ben@whitepixels.com.au>
 * @date    10/04/14
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @copyright   Copyright (c) 2014 White Pixels 
 */
 
class ProjectTableSeeder extends Seeder {

    public function run()
    {
        DB::table('projects')->delete();

        $seeds = array(
                array(
                        'name' => "Station 1",
                        'number' => "ST-001",
                        'description' => "A first station",
                        'manager' => "Jane Doe",
                        'budget' => 34500,
               ),
                array(
                        'name' => "Station 2",
                        'number' => "ST-002",
                        'description' => "A first station",
                        'manager' => "Jane Doe",
                        'budget' => 37500,
                ),
                array(
                        'name' => "Station 3",
                        'number' => "ST-003",
                        'description' => "A first station",
                        'manager' => "Jane Doe",
                        'budget' => 74500,
                )

        );

        foreach($seeds as $seed ){
            Project::create($seed);
        }
        $this->command->info( count($seeds) . ' projects created');
    }

}
 