<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//Create the table
        Schema::create('projects', function($table)
        {
            $table->increments('id');
            $table->string('number')->unique();
            $table->string('name');
            $table->string('manager');
            $table->text('description');
            $table->decimal('budget');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('projects');
	}

}
